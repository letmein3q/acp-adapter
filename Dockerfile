FROM golang:latest

RUN mkdir -p /go/src/acp-adapter

RUN mkdir /etc/pass_tsf/

ENV GO111MODULE on

WORKDIR /go/src/acp-adapter

COPY . /go/src/acp-adapter/

#RUN echo "qcloudAdmin" > /etc/pass_tsf/accountname

#RUN echo "Alauda@2020" > /etc/pass_tsf/password

RUN go build -mod=vendor main.go

EXPOSE 8080

CMD /go/src/acp-adapter/main
