package main

import (
	"solution/acp-adapter/controller"

	"github.com/kataras/iris"
	"github.com/kataras/iris/middleware/logger"
	"github.com/kataras/iris/middleware/recover"
	"github.com/kataras/iris/mvc"
)

func newApp() *iris.Application {
	app := iris.New()
	app.Use(recover.New())
	app.Use(logger.New())
	app.OnErrorCode(iris.StatusInternalServerError, internalServerError)
	app.OnErrorCode(iris.StatusForbidden, forbidden)
	mvc.New(app.Party("/sync")).Handle(controller.NewAdapterController())
	// mvc.New(app.Party("/tsf/metrics")).Handle(controller.NewMetricsController())
	return app
}

func internalServerError(ctx iris.Context) {
	errMessage := ctx.Values().GetString("error")
	if errMessage != "" {
		ctx.Writef("Internal server error: %s", errMessage)
		return
	}
	ctx.Writef("(Unexpected) internal server error")
}

func forbidden(ctx iris.Context) {
	errMessage := ctx.Values().GetString("error")
	if errMessage != "" {
		ctx.Writef("UnauthenticatedAccess: %s", errMessage)
		return
	}
	ctx.Writef("UnauthenticatedAccess")
}

func main() {
	app := newApp()
	app.Run(iris.Addr("127.0.0.1:8081"))
	// app.Run(iris.Addr(":8080"))
}
