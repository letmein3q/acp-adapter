package utils

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"solution/acp-adapter/config"
	"solution/acp-adapter/types/tsft"
	"strings"
	"sync"
	"time"
)

var loger = config.Logger

type Request interface {
	Path(path string) *request
}

type request struct {
	Host     string
	path     string
	UserName string
	PassWord string
	Cookie   *http.Cookie
	mux      sync.Mutex
}

func (r *request) Path(path string) *request {
	r.path = path
	return r
}

func (r *request) Get(body io.Reader) ([]byte, error) {
	request, err := http.NewRequest("GET", r.Host+r.path, body)
	if err != nil {
		return nil, err
	}

	request.Header.Set("Alauda-User", "acp")
	request.Header.Set("Content-Type", "application/json")
	if r.Cookie != nil {
		request.AddCookie(r.Cookie)
	}

	client := http.Client{}
	resp, err := client.Do(request)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	tmp, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return tmp, nil
}

func (r *request) Post(body io.Reader) ([]byte, error) {
	request, err := http.NewRequest("POST", r.Host+r.path, body)
	if err != nil {
		return nil, err
	}

	request.Header.Set("Alauda-User", "acp")
	request.Header.Set("Content-Type", "application/json")
	if r.Cookie != nil {
		request.AddCookie(r.Cookie)
	}

	client := http.Client{}
	resp, err := client.Do(request)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	tmp, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return tmp, nil
}

type login struct {
	Opt         string `json:"opt"`
	AccountName string `json:"accountName"`
	Password    string `json:"password"`
	UID         string `json:"uid"`
}

func (r *request) UpdateToken() {
	for {
		// Get token
		un, err := ioutil.ReadFile("/etc/pass_tsf/accountname")
		if err != nil {
			loger.Fatal(err)
		}
		pwd, err := ioutil.ReadFile("/etc/pass_tsf/password")
		if err != nil {
			loger.Fatal(err)
		}
		UserName := strings.Replace(string(un), "\n", "", 1)
		PassWord := strings.Replace(string(pwd), "\n", "", 1)

		r.setToken(UserName, PassWord)
		time.Sleep(time.Duration(4) * time.Hour)
	}
}

func Newrequest(hs string) *request {
	tmp := &request{
		Host: hs,
	}
	go tmp.UpdateToken()

	return tmp
}

func (r *request) setToken(username, password string) {
	// getToken
	body := login{
		Opt:         "login",
		AccountName: username,
		Password:    password,
	}
	js, _ := json.Marshal(body)
	jb := bytes.NewReader(js)

	var data tsft.TsfResult
	result, err := r.Path("/accountDispatch/login").Post(jb)
	if err != nil {
		loger.Fatal(err)
	}

	json.Unmarshal(result, &data)
	if data.Response.Error.Message != "" {
		err := errors.New("code: " + data.Response.Error.Code + ", message: " + data.Response.Error.Message)
		loger.Fatal(err)
	}
	r.mux.Lock()
	r.Cookie = &http.Cookie{Name: "accessToken", Value: data.Response.Result.Token}

	// registry token
	var data2 tsft.TsfResult
	body2 := login{
		Opt: "chooseUser",
		UID: "1",
	}
	js2, _ := json.Marshal(body2)
	jb2 := bytes.NewReader(js2)
	result2, err := r.Path("/accountDispatch/login").Post(jb2)
	json.Unmarshal(result2, &data2)
	if data2.Response.Error.Message != "" {
		err := errors.New("code: " + data2.Response.Error.Code + ", message: " + data2.Response.Error.Message)
		loger.Fatal(err)
	}
	if err != nil {
		loger.Fatal(err)
	}

	loger.Info("Token is " + data.Response.Result.Token)
	r.mux.Unlock()
}
