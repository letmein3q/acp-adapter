package controller

import (
	"errors"
	"solution/acp-adapter/types/acpt"

	"github.com/kataras/iris"
	"k8s.io/api/core/v1"
)

func (c *AdapterController) DeleteNodes(ctx iris.Context) {
	requestID := ctx.GetHeader("RequestID")
	p := &acpt.SyncNode{}
	if err := ctx.ReadJSON(p); err != nil {
		c.logger(requestID, "acp").Error(err)
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}
	c.logger(requestID, "controller-requestBody").Debug(p)

	instanceID := c.getInstanceID(p.Node)
	clusterID, err := c.getClusterID(instanceID)
	c.logger(requestID, "tsf").Debug(instanceID, clusterID)
	if err != nil {
		c.logger(requestID, "tsf").Error(err)
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}

	result, err := c.Model.Node().DeleteNode(clusterID, instanceID)
	if err != nil {
		c.logger(requestID, "tsf").Error(err)
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}
	c.logger(requestID, "tsf").Debug(result)
	c.logger(requestID, "").Infof("delete node %v successd, request_id of tsf is %v",
		clusterID, result.Response.RequestID)
	ctx.StatusCode(iris.StatusOK)
}

func (c *AdapterController) getInstanceID(n v1.Node) string {
	for _, v := range n.Status.Addresses {
		if v.Type == "InternalIP" {
			return v.Address
		}
	}
	return ""
}

func (c *AdapterController) getClusterID(instanceID string) (string, error) {
	r, err := c.Model.Cluster().ListCluster()
	if err != nil {
		return "", err
	}
	for _, v := range r.Response.Result.Content {
		c.logger("", "").Debug(v.ClusterID)
		r1, err := c.Model.Node().ListNode(v.ClusterID)
		if err != nil {
			return "", err
		}
		for _, n := range r1.Response.Result.Content {
			if n.InstanceID == instanceID {
				return v.ClusterID, nil
			}
		}
	}

	return "", errors.New("can't find clusterID for the node")
}
