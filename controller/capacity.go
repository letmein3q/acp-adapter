package controller

import "github.com/kataras/iris"

// GetResources is part of tsf's overview
func (c *AdapterController) GetCapacity(ctx iris.Context) {
	requestID := ctx.GetHeader("RequestID")
	result, err := c.Model.OverView().DescribeOverviewResource()
	if err != nil {
		c.logger(requestID, "tsf").Error(err)
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}
	c.logger(requestID, "tsf").Debug(result)

	result2, err := c.Model.OverView().DescribeOverviweService()
	if err != nil {
		c.logger(requestID, "tsf").Error(err)
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}
	c.logger(requestID, "tsf").Debug(result2)

	appc := result.Response.Result.ApplicationCount
	rmsc := result2.Response.Result.RunMicroserviceCount
	rs := make(map[string]map[string]int, 0)
	rs["result"] = make(map[string]int, 2)
	rs["result"]["ApplicationCount"] = appc
	rs["result"]["RunMicroserviceCount"] = rmsc
	ctx.JSON(rs)
	ctx.StatusCode(iris.StatusOK)
}
