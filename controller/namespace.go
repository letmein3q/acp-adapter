package controller

import (
	"bytes"
	"io/ioutil"
	"solution/acp-adapter/types/acpt"

	"github.com/kataras/iris"
)

func (c *AdapterController) getNamespaceRequestBody(ctx iris.Context, requestID string) (*acpt.SyncNameSpace, error) {
	// 用来核实controller发过来的数据是否正确，debug用的
	body, err := ioutil.ReadAll(ctx.Request().Body)
	if err == nil {
		defer ctx.Request().Body.Close()
		buf := bytes.NewBuffer(body)
		ctx.Request().Body = ioutil.NopCloser(buf)
		c.logger("987654321", "old-data").Debug(string(body))
	}

	p := &acpt.SyncNameSpace{}
	if err := ctx.ReadJSON(p); err != nil {
		return p, err
	}

	c.logger(requestID, "controller-requestBody").Debug(p)
	return p, nil
}

// PutNamespaces means curl -X PUT /sync/namespaces
func (c *AdapterController) PutNamespaces(ctx iris.Context) {
	requestID := ctx.GetHeader("RequestID")
	p, err := c.getNamespaceRequestBody(ctx, requestID)
	if err != nil {
		c.logger(requestID, "acp").Error(err)
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}

	namespaceName := p.Namespace.ObjectMeta.Name
	cpClusterID := ctx.GetHeader("cluster")
	result, err := c.Model.NameSpace().CreateNamespace(cpClusterID, namespaceName)
	if err != nil {
		c.logger(requestID, "tsf").Error(err)
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}
	c.logger(requestID, "tsf").Debug(result)
	c.logger(requestID, "").Infof("create namespace %v successd, request_id of tsf is %v",
		namespaceName, result.Response.RequestID)
	ctx.StatusCode(iris.StatusOK)
}

// DeleteNamespaces means curl -X DELETE /sync/namespaces
func (c *AdapterController) DeleteNamespaces(ctx iris.Context) {
	requestID := ctx.GetHeader("RequestID")
	p, err := c.getNamespaceRequestBody(ctx, requestID)
	if err != nil {
		c.logger(requestID, "acp").Error(err)
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}

	namespaceName := p.Namespace.ObjectMeta.Name
	cpClusterID := ctx.GetHeader("cluster")

	result, err := c.Model.NameSpace().DeleteNamespace(namespaceName, cpClusterID)
	if err != nil {
		c.logger(requestID, "tsf").Error(err)
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}
	c.logger(requestID, "tsf").Debug(result)
	c.logger(requestID, "").Infof("delete namespace %v successd, request_id of tsf is %v",
		namespaceName, result.Response.RequestID)
	ctx.StatusCode(iris.StatusOK)
}
