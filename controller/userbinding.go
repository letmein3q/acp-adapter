package controller

import (
	"bytes"
	"io/ioutil"
	"solution/acp-adapter/types/acpt"

	"github.com/kataras/iris"
	"github.com/spf13/viper"
)

func (c *AdapterController) PutUserbindings(ctx iris.Context) {
	requestID := ctx.GetHeader("RequestID")
	body, err := ioutil.ReadAll(ctx.Request().Body)
	if err == nil {
		defer ctx.Request().Body.Close()
		buf := bytes.NewBuffer(body)
		ctx.Request().Body = ioutil.NopCloser(buf)
		c.logger(requestID, "old-data").Debug(string(body))
	}

	p := &acpt.SyncUser{}
	if err := ctx.ReadJSON(p); err != nil {
		c.logger(requestID, "acp").Error(err)
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}

	c.logger(requestID, "controller-requestBody").Debug(p)

	// acp的项目名对应tsf的projectID
	projectID := p.UserBinding.Labels[viper.GetString("LABEL_BASE_DOMAIN")+"/project"]
	if projectID == "" {
		c.logger(requestID, "acp").Infof("this userbinding %v doesn't have projectID, discard", p.UserBinding.Name)
		ctx.StatusCode(iris.StatusOK)
		return
	}
	userID := p.UserBinding.Annotations["auth."+viper.GetString("LABEL_BASE_DOMAIN")+"/user.email"]

	result, err := c.Model.User().AddProjectUser(projectID, userID)
	if err != nil {
		c.logger(requestID, "tsf").Error(err)
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}
	c.logger(requestID, "").Infof("add user %v into project %v successd, request_id of tsf is %v",
		userID, projectID, result.Response.RequestID)
	ctx.StatusCode(iris.StatusOK)
}

func (c *AdapterController) DeleteUserbindings(ctx iris.Context) {
	requestID := ctx.GetHeader("RequestID")
	body, err := ioutil.ReadAll(ctx.Request().Body)
	if err == nil {
		defer ctx.Request().Body.Close()
		buf := bytes.NewBuffer(body)
		ctx.Request().Body = ioutil.NopCloser(buf)
		c.logger(requestID, "old-data").Debug(string(body))
	}

	p := &acpt.SyncUser{}
	if err := ctx.ReadJSON(p); err != nil {
		c.logger(requestID, "acp").Error(err)
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}

	c.logger(requestID, "controller-requestBody").Debug(p)

	projectID := p.UserBinding.Labels[viper.GetString("LABEL_BASE_DOMAIN")+"/project"]
	userID := p.UserBinding.Annotations["auth."+viper.GetString("LABEL_BASE_DOMAIN")+"/user.email"]

	result, err := c.Model.User().DeleteProjectUser(projectID, userID)
	if err != nil {
		c.logger(requestID, "tsf").Error(err)
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}
	c.logger(requestID, "").Infof("delete user %v into project %v successd, request_id of tsf is %v",
		userID, projectID, result.Response.RequestID)
	ctx.StatusCode(iris.StatusOK)
}
