package controller

import (
	"bytes"
	"io/ioutil"
	"solution/acp-adapter/types/acpt"

	"github.com/kataras/iris"
)

func (c *AdapterController) getClusterRequestBody(ctx iris.Context, requestID string) (*acpt.SyncCluster, error) {
	// 用来核实controller发过来的数据是否正确，debug用的
	body, err := ioutil.ReadAll(ctx.Request().Body)
	if err == nil {
		defer ctx.Request().Body.Close()
		buf := bytes.NewBuffer(body)
		ctx.Request().Body = ioutil.NopCloser(buf)
		c.logger("987654321", "old-data").Debug(string(body))
	}

	p := &acpt.SyncCluster{}
	if err := ctx.ReadJSON(p); err != nil {
		return p, err
	}

	c.logger(requestID, "controller-requestBody").Debug(p)
	return p, nil
}

// PutClusters means curl -X PUT /sync/Clusters
func (c *AdapterController) PutClusters(ctx iris.Context) {
	requestID := ctx.GetHeader("RequestID")
	p, err := c.getClusterRequestBody(ctx, requestID)
	if err != nil {
		c.logger(requestID, "acp").Error(err)
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}

	cpClusterID := p.Cluster.ObjectMeta.Name
	clusterName := p.Cluster.Annotations["cpaas.io/display-name"]
	result, err := c.Model.Cluster().CreateCluster(cpClusterID, clusterName)
	if err != nil {
		c.logger(requestID, "tsf").Error(err)
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}
	c.logger(requestID, "tsf").Debug(result)
	c.logger(requestID, "").Infof("create cluster %v successd, request_id of tsf is %v",
		cpClusterID, result.Response.RequestID)
	ctx.StatusCode(iris.StatusOK)
}

// DeleteClusters means curl -X DELETE /sync/Clusters
func (c *AdapterController) DeleteClusters(ctx iris.Context) {
	requestID := ctx.GetHeader("RequestID")
	p, err := c.getClusterRequestBody(ctx, requestID)
	if err != nil {
		c.logger(requestID, "acp").Error(err)
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}

	// acp的集群名对应tsf的集群ID
	cpClusterID := p.Cluster.ObjectMeta.Name

	result, err := c.Model.Cluster().DeleteCluster(cpClusterID)
	if err != nil {
		c.logger(requestID, "tsf").Error(err)
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}
	c.logger(requestID, "tsf").Debug(result)
	c.logger(requestID, "").Infof("delete cluster %v successd, request_id of tsf is %v",
		cpClusterID, result.Response.RequestID)
	ctx.StatusCode(iris.StatusOK)
}
