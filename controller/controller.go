package controller

import (
	"solution/acp-adapter/config"
	"solution/acp-adapter/models"
	"unicode/utf8"

	"github.com/sirupsen/logrus"
)

var (
	model = models.NewModel()
)

// AdapterController is used to recevie data from event of ace controller
type AdapterController struct {
	Model  models.Model
	Logger *logrus.Logger
}

// MetricsController is used to recevie data from who want to get tsf metrics
type MetricsController struct {
	Model  models.Model
	Logger *logrus.Logger
}

func NewAdapterController() *AdapterController {
	return &AdapterController{
		Model:  model,
		Logger: config.Logger,
	}
}

func NewMetricsController() *MetricsController {
	return &MetricsController{
		Model:  model,
		Logger: config.Logger,
	}
}

func (c *AdapterController) logger(requestID, phase string) *logrus.Entry {
	return c.Logger.WithFields(logrus.Fields{"request_id": requestID, "phase": phase})
}

func (c *MetricsController) logger(requestID, phase string) *logrus.Entry {
	return c.Logger.WithFields(logrus.Fields{"request_id": requestID, "phase": phase})
}

func SubStrRunes(s string, length int) string {
	if utf8.RuneCountInString(s) > length {
		rs := []rune(s)
		return string(rs[:length])
	}

	return s
}
