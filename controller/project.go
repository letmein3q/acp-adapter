package controller

import (
	"bytes"
	"io/ioutil"
	"solution/acp-adapter/types/acpt"

	"github.com/kataras/iris"
	"github.com/spf13/viper"
)

func (c *AdapterController) PutProjects(ctx iris.Context) {
	requestID := ctx.GetHeader("RequestID")
	body, err := ioutil.ReadAll(ctx.Request().Body)
	if err == nil {
		defer ctx.Request().Body.Close()
		buf := bytes.NewBuffer(body)
		ctx.Request().Body = ioutil.NopCloser(buf)
		c.logger(requestID, "old-data").Debug(string(body))
	}

	p := &acpt.SyncProject{}
	if err := ctx.ReadJSON(p); err != nil {
		c.logger(requestID, "acp").Error(err)
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}
	c.logger(requestID, "controller-requestBody").Debug(p)

	// acp的项目名对应tsf的projectID
	projectID := p.Project.ObjectMeta.Name
	projectName := p.Project.Annotations[viper.GetString("LABEL_BASE_DOMAIN")+"/display-name"]
	if projectName == "" {
		projectName = projectID
	}
	projectDesc := SubStrRunes(p.Project.Annotations[viper.GetString("LABEL_BASE_DOMAIN")+"/description"], 200)
	c.logger(requestID, "acp").Debugf("projectId=%v, projectName=%v, projectDesc=%v", projectID, projectName, projectDesc)

	update, err := c.isUpdate(projectID)
	if err != nil {
		c.logger(requestID, "tsf").Error(err)
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}

	if update {
		result, err := c.Model.Project().UpdateProject(projectID, projectName, projectDesc)
		if err != nil {
			c.logger(requestID, "tsf").Error(err)
			ctx.StatusCode(iris.StatusInternalServerError)
			return
		}
		c.logger(requestID, "tsf").Debug(result)
		c.logger(requestID, "").Infof("Update Project %v successd, request_id of tsf is %v",
			projectName, result.Response.RequestID)
		ctx.StatusCode(iris.StatusOK)
		return
	}

	result, err := c.Model.Project().CreateProject(projectID, projectName, projectDesc)
	if err != nil {
		c.logger(requestID, "tsf").Error(err)
		ctx.StatusCode(iris.StatusInternalServerError)
		return
	}
	c.logger(requestID, "").Infof("Create Project %v successd, request_id of tsf is %v",
		projectName, result.Response.RequestID)
	ctx.StatusCode(iris.StatusOK)
}

// update is true create is false
func (c *AdapterController) isUpdate(projectID string) (bool, error) {
	result, err := c.Model.Project().ListProjects()
	if err != nil {
		return false, err
	}

	for _, v := range result.Response.Result.Content {
		if v.ProjectID == projectID {
			return true, nil
		}
	}

	return false, nil
}
