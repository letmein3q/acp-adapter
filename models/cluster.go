package models

import (
	"solution/acp-adapter/types/tsft"
)

type Cluster interface {
	ListCluster() (tsft.TsfResult, error)
	CreateCluster(cpClusterID, clusterName string) (tsft.TsfResult, error)
	DeleteCluster(cpclusterID string) (tsft.TsfResult, error)
}

type cluster struct{}

func (c *cluster) ListCluster() (tsft.TsfResult, error) {
	body := tsft.NewTsfBasicRequestBody("DescribeClusters", "tsf")
	body.Data.Limit = 50
	result, error := ToTsf(body)
	return result, error
}

func (c *cluster) CreateCluster(cpClusterID, clusterName string) (tsft.TsfResult, error) {
	body := tsft.NewTsfBasicRequestBody("CreateCluster", "tsf")
	body.Data.CpClusterID = cpClusterID
	body.Data.ClusterName = clusterName
	result, error := ToTsf(body)
	return result, error
}

func (c *cluster) DeleteCluster(cpClusterID string) (tsft.TsfResult, error) {
	body := tsft.NewTsfBasicRequestBody("DeleteCluster", "tsf")
	body.Data.CpClusterID = cpClusterID
	result, error := ToTsf(body)
	return result, error
}
