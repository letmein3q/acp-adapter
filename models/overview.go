package models

import (
	"fmt"
	"solution/acp-adapter/types/tsft"
)

type OverView interface {
	DescribeOverviewResource() (tsft.TsfResult, error)
	DescribeOverviweService() (tsft.TsfResult, error)
}

type overview struct{}

func (o *overview) DescribeOverviewResource() (tsft.TsfResult, error) {
	body := tsft.NewTsfBasicRequestBody("DescribeOverviewResource", "tsf")
	body.Data.Version = "2018-03-26"
	result, error := ToTsf(body)
	fmt.Println(result)
	return result, error
}

func (o *overview) DescribeOverviweService() (tsft.TsfResult, error) {
	body := tsft.NewTsfBasicRequestBody("DescribeOverviweService", "tsf")
	body.Data.Version = "2018-03-26"
	result, error := ToTsf(body)
	return result, error
}
