package models

import (
	"solution/acp-adapter/config"
)

var (
	Logger = config.Logger
)

type Model interface {
	Project() Project
	User() User
	Cluster() Cluster
	Node() Node
	NameSpace() NameSpace
	OverView() OverView
}

type model struct{}

func (m *model) Project() Project {
	return &project{}
}

func (m *model) User() User {
	return &user{}
}

func (m *model) Cluster() Cluster {
	return &cluster{}
}

func (m *model) Node() Node {
	return &node{}
}

func (m *model) NameSpace() NameSpace {
	return &namespace{}
}

func (m *model) OverView() OverView {
	return &overview{}
}

func NewModel() Model {
	return new(model)
}
