package models

import (
	"solution/acp-adapter/types/tsft"
)

type Node interface {
	ListNode(clusterID string) (tsft.TsfResult, error)
	DeleteNode(clusterID, instanceID string) (tsft.TsfResult, error)
}

type node struct{}

func (n *node) ListNode(clusterID string) (tsft.TsfResult, error) {
	body := tsft.NewTsfBasicRequestBody("DescribeClusterInstances", "tsf")
	body.Data.ClusterID = clusterID
	body.Data.Limit = 1000
	result, error := ToTsf(body)
	return result, error
}

func (n *node) DeleteNode(clusterID, instanceID string) (tsft.TsfResult, error) {
	body := tsft.NewTsfBasicRequestBody("RemoveInstance", "tsf")
	body.Data.ClusterID = clusterID
	body.Data.InstanceID = instanceID
	result, error := ToTsf(body)
	return result, error
}
