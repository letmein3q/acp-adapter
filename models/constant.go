package models

import (
	"bytes"
	"encoding/json"
	"errors"
	"solution/acp-adapter/types/tsft"
	"solution/acp-adapter/utils"

	"github.com/spf13/viper"
)

const (
	Path = "/apiDispatch/v3"
)

var (
	Request = utils.Newrequest(viper.GetString("TSF_ENDPOINT"))
)

func ToTsf(body tsft.TsfRequestBody) (tsft.TsfResult, error) {
	js, _ := json.Marshal(body)
	jb := bytes.NewReader(js)

	var tmp tsft.TsfResult

	result, err := Request.Path(Path).Post(jb)
	if err != nil {
		return tmp, err
	}
	json.Unmarshal(result, &tmp)
	if tmp.Response.Error.Message != "" {
		return tmp, errors.New("code: " + tmp.Response.Error.Code + ", message: " + tmp.Response.Error.Message)
	}

	return tmp, nil
}
