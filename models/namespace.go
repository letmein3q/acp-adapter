package models

import (
	"solution/acp-adapter/types/tsft"
)

type NameSpace interface {
	CreateNamespace(cpClusterID, namespaceName string) (tsft.TsfResult, error)
	DeleteNamespace(namespaceName, cpClusterID string) (tsft.TsfResult, error)
}

type namespace struct{}

func (n *namespace) CreateNamespace(cpClusterID, namespaceName string) (tsft.TsfResult, error) {
	body := tsft.NewTsfBasicRequestBody("CreateNamespace", "tsf")
	body.Data.CpClusterID = cpClusterID
	body.Data.NamespaceName = namespaceName
	result, error := ToTsf(body)
	return result, error
}

func (n *namespace) DeleteNamespace(namespaceName, cpClusterID string) (tsft.TsfResult, error) {
	body := tsft.NewTsfBasicRequestBody("DeleteNamespace", "tsf")
	body.Data.CpClusterID = cpClusterID
	body.Data.NamespaceName = namespaceName
	result, error := ToTsf(body)
	return result, error
}
