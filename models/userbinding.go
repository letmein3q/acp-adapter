package models

import (
	"solution/acp-adapter/types/tsft"
)

type User interface {
	ListUser(userName string) (tsft.TsfResult, error)
	AddProjectUser(projectID, userID string) (tsft.TsfResult, error)
	DeleteProjectUser(projectID, userID string) (tsft.TsfResult, error)
}

type user struct{}

func (u *user) ListUser(userName string) (tsft.TsfResult, error) {
	body := tsft.NewTsfBasicRequestBody("DescribeUsers", "access-auth")
	body.Data.UserAppID = "100000000"
	body.Data.SearchWord = userName
	result, error := ToTsf(body)
	return result, error
}

func (u *user) AddProjectUser(projectID, userID string) (tsft.TsfResult, error) {
	var UserNameList = []string{userID}
	body := tsft.NewTsfBasicRequestBody("AddProjectUserSynAcp", "access-auth")
	body.Data.ProjectAppID = "100000000"
	body.Data.ProjectID = projectID
	body.Data.UserNameList = UserNameList
	result, error := ToTsf(body)
	return result, error
}

func (u *user) DeleteProjectUser(projectID, userID string) (tsft.TsfResult, error) {
	body := tsft.NewTsfBasicRequestBody("DeleteProjectUserSynAcp", "access-auth")
	body.Data.ProjectID = projectID
	body.Data.UserName = userID
	result, error := ToTsf(body)
	return result, error
}
