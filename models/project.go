package models

import (
	"solution/acp-adapter/types/tsft"
)

type Project interface {
	ListProjects() (tsft.TsfResult, error)
	CreateProject(projectID, projectName, projectDesc string) (tsft.TsfResult, error)
	UpdateProject(projectID, projectName, projectDesc string) (tsft.TsfResult, error)
}

type project struct{}

func (p *project) ListProjects() (tsft.TsfResult, error) {
	body := tsft.NewTsfBasicRequestBody("DescribeProjects", "access-auth")
	body.Data.Limit = 10000
	result, error := ToTsf(body)
	return result, error
}

func (p *project) CreateProject(projectID, projectName, projectDesc string) (tsft.TsfResult, error) {
	body := tsft.NewTsfBasicRequestBody("CreateProject", "access-auth")
	body.Data.ProjectAppID = "100000000"
	body.Data.ProjectID = projectID
	body.Data.ProjectName = projectName
	body.Data.ProjectDesc = projectDesc
	result, error := ToTsf(body)
	return result, error
}

func (p *project) UpdateProject(projectID, projectName, projectDesc string) (tsft.TsfResult, error) {
	body := tsft.NewTsfBasicRequestBody("ModifyProject", "access-auth")
	body.Data.ProjectAppID = "100000000"
	body.Data.ProjectID = projectID
	body.Data.ProjectName = projectName
	body.Data.ProjectDesc = projectDesc
	result, error := ToTsf(body)
	return result, error
}
