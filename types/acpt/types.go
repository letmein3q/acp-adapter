package acpt

import (
	"k8s.io/api/core/v1"
)

// SyncProject is used to parse project data from acp controller
type SyncProject struct {
	Project Project `json:"project"`
}

// SyncUser is used to parse user data from acp controller
type SyncUser struct {
	UserBinding UserBinding `json:"userbinding"`
}

// SyncCluster is used to parse cluster data from acp controller
type SyncCluster struct {
	Cluster Cluster `json:"cluster"`
}

// SyncNode is used to parse node data from acp controller
type SyncNode struct {
	Node v1.Node `json:"node"`
}

// SyncNameSpace is used to parse namespace data from acp controller
type SyncNameSpace struct {
	Namespace v1.Namespace `json:"namespace"`
}
