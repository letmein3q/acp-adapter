package tsft

// TsfResult is used to parse project list of tsf
type TsfResult struct {
	Response response
}

type response struct {
	Error     er
	RequestID string `json:"RequestId"`
	Result    result `json:"Result"`
}

type er struct {
	Code    string `json:"Code"`
	Message string `json:"Message"`
}

type result struct {
	Token                 string
	Content               []content
	ApplicationCount      int `json:"ApplicationCount"`
	ClusterCount          int `json:"ClusterCount"`
	GroupCount            int `json:"GroupCount"`
	InstanceCount         int `json:"InstanceCount"`
	NamespaceCount        int `json:"NamespaceCount"`
	OffInstanceCount      int `json:"OffInstanceCount"`
	RunInstanceCount      int `json:"RunInstanceCount"`
	UnNormalInstanceCount int `json:"UnNormalInstanceCount"`
	RunMicroserviceCount  int `json:"RunMicroserviceCount"`
}

type content struct {
	// for project
	ProjectID string `json:"ProjectId"`
	// for user
	UserID   string `json:"UserId"`
	UserName string `json:"UserName"`
	// for cluster
	ClusterID   string `json:"ClusterId"`
	ClusterName string `json:"ClusterName"`
	// for node
	InstanceID string `json:"InstanceId"`
}

// TsfRequestBody is the request body that send to tsf
type TsfRequestBody struct {
	Action      string `json:"action"`
	ServiceType string `json:"serviceType"`
	RegionID    int    `json:"regionId"`
	Data        data   `json:"data"`
}

type data struct {
	SearchWord   string `json:"searchWord"`   //listProject, listUser
	Offset       int    `json:"offset"`       //listProject, listUser
	Limit        int    `json:"limit"`        //listProject, listUser
	ProjectAppID string `json:"projectAppId"` //listUser, createProject, updateProject
	ProjectName  string `json:"projectName"`  //createProject, updateProject
	ProjectID    string `json:"projectId"`    //createProject, updateProject, addProjectUser
	ProjectDesc  string `json:"projectDesc"`  //createProject, updateProject
	// UserIDList   []string `json:"userIdList"`   //addProjectUser
	UserNameList  []string `json:"userNameList"`  //addProjectUser
	UserAppID     string   `json:"userAppId"`     //listUser
	UserName      string   `json:"userName"`      //deleteProjectUser
	ClusterID     string   `json:"clusterId"`     //deleteNode
	CpClusterID   string   `json:"cpClusterId"`   //deleteCluster
	ClusterName   string   `json:"clusterName"`   //createCluster
	InstanceID    string   `json:"instanceId"`    //deleteNode
	NamespaceName string   `json:"namespaceName"` //createNameSpace
	Version       string   `json:"Version"`       //DescribeOverviwe
}

func NewTsfBasicRequestBody(action, serviceType string) TsfRequestBody {
	tmp := TsfRequestBody{
		Action:      action,
		ServiceType: serviceType,
		RegionID:    1,
		Data:        data{},
	}
	return tmp
}

// NewTsfListProjectRequestBody is used to gen a list project body
// data.SearchWord is variable
// func NewTsfListProjectRequestBody() TsfRequestBody {
// 	tmp := TsfRequestBody{
// 		Action:      "DescribeProjects",
// 		ServiceType: "access-auth",
// 		RegionID:    1,
// 		Data: data{
// 			Offset:       0,
// 			Limit:        100,
// 			ProjectAppID: "",
// 		},
// 	}
// 	return tmp
// }

// NewTsfCreateProjectRequestBody is used to gen a create project body
// Action and createdata.ProjectName is variable
// createdata.ProjectAppID is constatn 100000000
// func NewTsfCUProjectRequestBody() TsfRequestBody {
// 	tmp := TsfRequestBody{
// 		Action:      "",
// 		ServiceType: "access-auth",
// 		RegionID:    1,
// 		Data: data{
// 			ProjectAppID: "100000000",
// 		},
// 	}
// 	return tmp
// }

// func NewTsfUserRequestBody() TsfRequestBody {
// 	tmp := TsfRequestBody{
// 		Action:      "",
// 		ServiceType: "access-auth",
// 		RegionID:    1,
// 		Data:        data{},
// 	}
// 	return tmp
// }
